//
//  AgeCalculator.swift
//  AgeCalculator
//
//  Created by MacGoPro on 15/10/2018.
//  Copyright © 2018 self. All rights reserved.
//

import Foundation

class AgeCalculator {
    
    func ageInYears(datee: String) -> String{
        let newDate = convertDate(datee: datee)
        let today = Date()
        let components = Set<Calendar.Component>([.minute, .hour, .day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: newDate, to: today)
        let string = "\(differenceOfDate)"
        print(string)
        return string
    }
    private func convertDate(datee: String) -> Date {
        let dateFormatr = DateFormatter()
        dateFormatr.dateFormat = "dd/MM/yyyy"
        dateFormatr.dateStyle = .short
        let date = dateFormatr.date(from: datee)
        return date!
    }
}
