# AgeCalculatorLib

[![CI Status](https://img.shields.io/travis/asn111/AgeCalculatorLib.svg?style=flat)](https://travis-ci.org/asn111/AgeCalculatorLib)
[![Version](https://img.shields.io/cocoapods/v/AgeCalculatorLib.svg?style=flat)](https://cocoapods.org/pods/AgeCalculatorLib)
[![License](https://img.shields.io/cocoapods/l/AgeCalculatorLib.svg?style=flat)](https://cocoapods.org/pods/AgeCalculatorLib)
[![Platform](https://img.shields.io/cocoapods/p/AgeCalculatorLib.svg?style=flat)](https://cocoapods.org/pods/AgeCalculatorLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AgeCalculatorLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AgeCalculatorLib'
```

## Author

asn111, asi@embrace-it.com

## License

AgeCalculatorLib is available under the MIT license. See the LICENSE file for more info.
